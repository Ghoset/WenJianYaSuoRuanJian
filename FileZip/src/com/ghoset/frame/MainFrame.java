package com.ghoset.frame;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

import com.ghoset.util.UtilMain;
/**
 * 主界面
 * @author Administrator
 *
 */
public class MainFrame extends JFrame {
	/**单例模式**/
	private MainFrame(){
		super("软件压缩工具Ghoset 0.1" );
		//居中显示和设置大小
		Toolkit toolkit = Toolkit.getDefaultToolkit().getDefaultToolkit();
		Dimension screenSize = toolkit.getScreenSize();
		int width = (int) screenSize.getWidth();
		int height = (int) screenSize.getHeight();
		this.setBounds((width - 900) / 2 , (height - 400) / 2 , 900 , 400 );
		
		/**美化界面和初始化界面**/
		try{
			String windows = "com.sun.java.swing.plaf.windows.WindowsLookAndFeel";
			UIManager.setLookAndFeel(windows);
			SwingUtilities.updateComponentTreeUI(this );
		}catch(Exception e ){
			e.printStackTrace();
		}
		
		initial();
		/**界面交互**/
		bindEvent();
		
		
		this.setVisible(true );
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE );
		
		
	}
	
	public static MainFrame newInstance (){
		return new MainFrame();
	}
	
	/**工具包**/
	UtilMain util = new UtilMain();
	
	/**所有控件**/
	private Box selBox = Box.createVerticalBox();
	private JPanel panel_src = new JPanel(new FlowLayout(FlowLayout.CENTER ) );
	private JPanel panel_base = new JPanel(new FlowLayout(FlowLayout.CENTER )  );
	
	private JLabel lb_srcFile = new JLabel("压缩的文件:" );
	private JLabel lb_baseFile = new JLabel("压缩到路径:" );	//这里我们指定让用户选择目录 , 我们来构造压缩文件
	private JTextField text_Scr = new JTextField(60 );
	private JTextField text_base = new JTextField(60 );
	private JButton btn_src = new JButton("点击选择" );
	private JButton btn_base = new JButton("点击压缩" );
	
	private JTable fileTable = new JTable();
	private DefaultTableModel model = new DefaultTableModel();
	private JScrollPane scroll = new JScrollPane(fileTable );
	
	private FileDialog openDialog;
	
	/**初始化界面**/
	private void initial(){
		this.setResizable(false );
		this.setLayout(new BorderLayout() );
		
		panel_src.add(lb_srcFile );
		panel_src.add(text_Scr );
		panel_src.add(btn_src );
		
		panel_base.add(lb_baseFile );
		panel_base.add(text_base );
		panel_base.add(btn_base );
		
		fileTable.setModel(model );;
		model.setColumnIdentifiers(new Object[]{
				"ID" , "文件名" , "类型" , "大小" , "修改日期" , "状态"
		});
		
		selBox.add(panel_src );
		selBox.add(panel_base );
		
		
		/*
		north;
		south;
		*/
		
		this.add(selBox , BorderLayout.NORTH );
		this.add(scroll , BorderLayout.CENTER );
		
	}
	
	
	/**压缩工具界面功能**/
	public void bindEvent(){
		
		//注意:这里当用户选择了文件 , 我们自动帮助它构造一个压缩的路径 , 用户可以修改 . 
		btn_src.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				JFileChooser chooser = new JFileChooser();
				//设置可以选择文件和目录 
				chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES );
				chooser.setCurrentDirectory(new File("C:/" ) );
				
				int ret = chooser.showOpenDialog(MainFrame.this );
				if(ret == JFileChooser.APPROVE_OPTION ){
					String fileName = chooser.getSelectedFile().getAbsolutePath();
					String baseName = null;		//获取这个目录 
					
					int index = fileName.lastIndexOf("." );								//如果这个是文件文件的话 , 那我们要重构存储的压缩文件路径
					if(index > 0 ){
						baseName = fileName.substring(0 , index ) + ".zip";
					}else{
						baseName = fileName + ".zip";
					}
					
					text_Scr.setText(fileName );
					text_base.setText(baseName );
					
				}
				
				
			}
		});
		
		
		btn_base.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				String fileName = text_Scr.getText();
				String baseName = text_base.getText();
				
				if(fileName.isEmpty() || baseName.isEmpty() ){
					JOptionPane.showMessageDialog(MainFrame.this , "必须输入指定路径!" );
					return;
				}
				
				boolean flag = false;
				File file = new File(fileName );
				
				if(file.isDirectory() ){
					flag = util.directoryZip(fileName , baseName );
					File[] files = file.listFiles();
					showToTable(files );
					
				}else{
					flag = util.fileZip(fileName , baseName );
				}
				
				if(flag ){
					JOptionPane.showMessageDialog(MainFrame.this , "压缩文件成功!" );
				}else{
					JOptionPane.showMessageDialog(MainFrame.this , "压缩文件失败!" );
				}
			}
		});
		
	}
	
	/**将文件显示到JTable中**/
	void showToTable(File[] files ){
		int id = 0;
		for(File v : files ){
			model.addRow(new Object[]{
				id , v.getName() , v.isDirectory() , v.length() , v.lastModified() , "压缩成功"
			});
			id++ ;
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
