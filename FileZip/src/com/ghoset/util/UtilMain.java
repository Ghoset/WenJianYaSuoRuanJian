package com.ghoset.util;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * 压缩程序算法
 * 支持两种压缩 文件和目录
 * @author Administrator
 *
 */
public class UtilMain {
	
	/**文件压缩**/
	public boolean fileZip(String srcFile , String baseFile ){
		
		if(srcFile == null || baseFile == null ){
			return false;
		}else if( srcFile.length() == 0 || baseFile.length() == 0 ){
			return false;
		}
		
		ZipOutputStream zipOutput = null;
		BufferedInputStream fileInput = null;
		//1.创建压缩文件对象
		try{
			zipOutput = new ZipOutputStream(new FileOutputStream(baseFile ) );
			fileInput = new BufferedInputStream(new FileInputStream(srcFile ) );
		
		//2. 创建压缩文件里面的文件
			zipOutput.putNextEntry(new ZipEntry(new File(srcFile).getName() ) );
			zipOutput.setComment("Hello ! You check the Ghoset ZipFile!" );
			
		//3. 读取文件数据写入到压缩文件中
			byte[] readBuf = new byte[fileInput.available() ];
			int ch = fileInput.read(readBuf );
			while(ch != 0 && ch != -1 ){
				zipOutput.write(readBuf , 0 , ch );
				ch = fileInput.read(readBuf );
			}
			return true;
		}catch(Exception e ){
			e.printStackTrace();
		} finally{
			try{
				zipOutput.flush();
				zipOutput.close();
				fileInput.close();
			}catch(Exception e ){
				e.printStackTrace();
			}
		}
		
	
		return false;
	}
	
	
	/**目标压缩**/
	public boolean directoryZip(String srcDirectory , String baseFile ){
		//1. 创建压缩对象
		try{
			ZipOutputStream zipOutput = new ZipOutputStream(new FileOutputStream(baseFile ) );
			BufferedInputStream fileInput;
		//2. 将文件夹里面的所有文件进行压缩.
			zipOutput.setComment("Hello ! You check the Ghoset ZipFile!" );
			File director = new File(srcDirectory );
			File[] files = director.listFiles();
			for(File v : files ){
				zipOutput.putNextEntry(new ZipEntry(
						director.getName() + File.separator + v.getName()		//separator表示"//"下级目录符
						));
				//因为里面可能有的不是文本文件 , 所以就这样设置字节流
				fileInput = new BufferedInputStream(new FileInputStream(v )  );
				
				byte[] buf = new byte[fileInput.available() ];
				int ch = fileInput.read(buf );
				while(ch != 0 && ch != -1 ){		// !=0解决的是没有数据的问题
					zipOutput.write(buf , 0 , ch );
					ch = fileInput.read(buf );
				}
				
				fileInput.close();
				zipOutput.flush();		//刷新缓冲区 , 防止没有将数据输出到外部
				
			}
			
			zipOutput.close();
			
			return true;
			
		}catch(Exception e ){
			e.printStackTrace();
		}
		return false;
	}

	
}
